#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on %(date)s

@author: %(username)s
"""
import numpy as np

#import pour la position du soleil
from astropy.coordinates import get_sun 
from astropy.time import Time

#import pour la gestion de ply
from plyfile import PlyData, PlyElement
import nuage

class Ref():
  
    def __init__(self,date,heure,georef,pathnuage,rayon,alpha,uR,uL,tO,mO,origine): # The constructor
        self.date=date
        self.heure=heure
        self.georef=georef
        self.pathnuage=pathnuage
        plydata = PlyData.read(self.pathnuage)  
        self.header=plydata
        self.langage='b'
        self.loc=[0,0,0]
        x,y,z=plydata.elements[0].data['x'],plydata.elements[0].data['y'],plydata.elements[0].data['z']
        red,green,blue=plydata.elements[0].data['red'],plydata.elements[0].data['green'],plydata.elements[0].data['blue']
        self.matcoor=np.vstack((np.array(x),np.array(y),np.array(z))).T
        self.colors=np.vstack((np.array(red),np.array(green),np.array(blue))).T
        self.nuage=nuage.Nuage(self.matcoor,mO)
        self.pts_ombre=[]
        self.pts_soleil=[]
        self.rayon=rayon
        self.alpha=alpha*np.pi/180
        self.fac_echelle=uR/uL
        self.type_ombre=tO
        
    def calculer_soleil(self):
        """Calcule la position du soleil en coordoonées géocentriques à partir des informations de temps"""
        time = [self.date+'T'+self.heure]
        t = Time(time, format='isot', scale='utc')
        res=get_sun(t).transform_to(ITRS())
        self.pos_soleil=[res.x.si.to_value()[0],res.y.si.to_value()[0],res.z.si.to_value()[0]]

      
    def calculer_soleil_loc(self):
        """A partir de la position du soleil calculée, la transforme en coordonnées locales"""
        X0 = 1 - self.fac_echelle*self.georef[0]
        Y0 = 0 - self.fac_echelle*self.georef[1]
        Z0 = 0 - self.fac_echelle*self.georef[2]
        X1 = 0 - self.fac_echelle*self.georef[3]
        Y1 = 1 - self.fac_echelle*self.georef[4]
        Z1 = 0 - self.fac_echelle*self.georef[5]
        A=np.array([[1,0,0, 0,self.fac_echelle*self.georef[2],-self.fac_echelle*self.georef[1]],
                   [0,1,0, -self.fac_echelle*self.georef[2],0,self.fac_echelle*self.georef[0]],
                   [0,0,1, self.fac_echelle*self.georef[1],-self.fac_echelle*self.georef[0],0],
                   [1,0,0, 0,self.fac_echelle*self.georef[5],-self.fac_echelle*self.georef[4]],
                   [0,1,0, -self.fac_echelle*self.georef[5],0,self.fac_echelle*self.georef[3]],
                   [0,0,1, self.fac_echelle*self.georef[4],-self.fac_echelle*self.georef[3],0]])
        B=np.array([X0,Y0,Z0,X1,Y1,Z1])
        X=np.linalg.solve(A,B)
        R=np.array([[ 1   ,-X[5], X[4]],
                   [ X[5], 1   ,-X[3]],
                   [-X[4], X[3], 1   ]])
        
        self.posloc_soleil=np.array([X[0],X[1],X[2]])+self.fac_echelle*R@self.pos_soleil
    
    def direction_soleil(self):
        """Calcule une direction locale par rapport au soleil"""
        self.direction=self.posloc_soleil-self.matcoor[0]#Le soleil est assez loin pour considérer une direction semblable pour tous les points
        self.direction= self.direction/np.linalg.norm(self.direction)
        
    def ouvrir_nuage(self):
        """Retourne la matrice des coordonnées"""
        return self.matcoor
    
    def process_nuage(self):
        """Active le calcul des ombres du nuage"""
        rayon,direction=self.rayon,self.direction
        #rayon,direction=3e-3,[1,1,1]#ligne de test rapide des calculs
        self.nuage.process(rayon,self.alpha, direction)
        
    def ombrage(self):
        """Crée deux attributs self.pts_ombre et self.pts_soleil qui recensent les points correspondants et leur couleur, modifiée si besoin"""
        pts_ombre,pts_soleil=[],[]
        nb=self.nuage.nbpoints
        if self.type_ombre == 0:
            for i in range(nb):
                c=self.colors[i]
                score_ombre=self.nuage.ombre[i]
                if score_ombre:
                        c[0] = int(0.5*c[0])#c=c*facteur_ombre 
                        c[1] = int(0.5*c[1])
                        c[2] = int(0.5*c[2])
                        pts_ombre.append( (self.nuage.matpoints[i,0],self.nuage.matpoints[i,1],self.nuage.matpoints[i,2],c[0],c[1],c[2]) )
                else:
                    pts_soleil.append((self.nuage.matpoints[i,0],self.nuage.matpoints[i,1],self.nuage.matpoints[i,2],c[0],c[1],c[2]))
        elif self.type_ombre == 1:
            max_ombre=np.max(self.nuage.ombre)
            for i in range(nb):
                c=self.colors[i]
                score_ombre=self.nuage.ombre[i]
                if score_ombre:
                        c[0] = int((1-0.5*score_ombre/max_ombre)*c[0])#c=c*facteur_ombre 
                        c[1] = int((1-0.5*score_ombre/max_ombre)*c[1])
                        c[2] = int((1-0.5*score_ombre/max_ombre)*c[2])
                        pts_ombre.append( (self.nuage.matpoints[i,0],self.nuage.matpoints[i,1],self.nuage.matpoints[i,2],c[0],c[1],c[2]) )
                else:
                    pts_soleil.append((self.nuage.matpoints[i,0],self.nuage.matpoints[i,1],self.nuage.matpoints[i,2],c[0],c[1],c[2]))
        self.pts_ombre = np.array(pts_ombre, dtype=[('x', 'f4'), ('y', 'f4'),('z', 'f4'),('red', 'u1'), ('green', 'u1'),('blue', 'u1')])
        self.pts_soleil = np.array(pts_soleil, dtype=[('x', 'f4'), ('y', 'f4'),('z', 'f4'),('red', 'u1'), ('green', 'u1'),('blue', 'u1')])

    def creer_nuages(self):
        """Crée deux fichiers.ply _ombre.ply et _soleil.ply à partir des attributs correspondants"""
        el_ombre =  PlyElement.describe(self.pts_ombre, 'vertex' )#, comments=['vertices'])
        PlyData([el_ombre], text=True).write(self.pathnuage.replace(".ply","_ombre.ply")) 
        el_soleil = PlyElement.describe(self.pts_soleil, 'vertex')
        PlyData([el_soleil], text=True).write(self.pathnuage.replace(".ply","_soleil.ply")) 


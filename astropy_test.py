#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on %(date)s

@author: %(username)s
"""
import gpsdatetime as g
from astropy.coordinates import get_sun 
from astropy.time import Time


if __name__=="__main__":
    tic = g.gpsdatetime()
    date,heure='11-02-2000','00:00:00.123456789'
    time = ['11-02-2000T00:00:00.123456789']
    test=[date+'T'+heure]
    print(test==time)
    """
    t = Time(time, format='isot', scale='utc')
    res=get_sun(t)#.to_string()
    df=res.to_table().to_pandas()
    print(df)
    print(df['dec'][0])
    """
    toc =  g.gpsdatetime()
    dt = toc-tic
    print("Duration : %.3fs" % dt)

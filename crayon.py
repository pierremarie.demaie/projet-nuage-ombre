#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on %(date)s

@author: %(username)s
"""
import numpy as np

class Crayon():
  
    def __init__(self,point,rayon,alpha,direction):
        self.origine = point
        self.direction = direction/np.linalg.norm(direction)
        self.tanalpha=np.tan(alpha)
        self.rayon=rayon
        
    def include(self,MP):
        """Fonction de test d'appartenance: le point doit être différent de l'origine,/
        il doit se trouver dans la bonne direction et être dans les limites du cylindre et du cone"""
        if (MP == [0,0,0]).all():
            return False
        MPu = np.dot(self.direction,MP)
        if MPu < 0:
            return False
        else:
            r = np.linalg.norm(np.cross(MP,self.direction))
            if r < self.rayon:
                r_cone = self.tanalpha*MPu
                if r<r_cone:
                    return True
            else: return False
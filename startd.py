# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 07:07:55 2022

@author: Pierre-Marie
"""
import numpy as np
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication
import sys
import infdesign
import ref

class Interface_Ombre(QtWidgets.QMainWindow, infdesign.Ui_MainWindow):
    def __init__(self, parent=None):
        super(Interface_Ombre, self).__init__(parent)
        self.setupUi(self)
    def connect(self):
        self.pushButton.clicked.connect(self.calculate)
        
    def calculate(self):
        """ Cette fonction récupère les contenus des différents champs et lance le programme quand l'utilisateur appuie sur le bouton
        """
        path=self.lineEdit.text()
        datetime=self.dateTimeEdit.text()
        date,heure=datetime.split()
        d,m,y=date.split('/')
        date=y+'-'+m+'-'+d
        rayon=float(self.doubleSpinBox_15.text().replace(',','.'))
        angle=float(self.doubleSpinBox_16.text().replace(',','.'))
        georef=[]
        georef.append(float(self.doubleSpinBox.text().replace(',','.')))
        georef.append(float(self.doubleSpinBox_2.text().replace(',','.')))
        georef.append(float(self.doubleSpinBox_3.text().replace(',','.')))
        georef.append(float(self.doubleSpinBox_4.text().replace(',','.')))
        georef.append(float(self.doubleSpinBox_5.text().replace(',','.')))
        georef.append(float(self.doubleSpinBox_6.text().replace(',','.')))
        Origine=[]
        Origine.append(float(self.doubleSpinBox_10.text().replace(',','.')))
        Origine.append(float(self.doubleSpinBox_11.text().replace(',','.')))
        Origine.append(float(self.doubleSpinBox_12.text().replace(',','.')))
        Ur=float(self.doubleSpinBox_13.text().replace(',','.'))
        Ul=float(self.doubleSpinBox_14.text().replace(',','.'))
        m_o=self.comboBox.currentIndex()
        t_o=self.comboBox_2.currentIndex()
        new=ref.Ref(date,heure,georef,path,rayon,angle,Ur,Ul,t_o,m_o,Origine)
        new.calculer_soleil()
        new.calculer_soleil_loc()
        new.direction_soleil()
        new.process_nuage()
        new.ombrage()
        new.creer_nuages()

        
    
        
        

def main():
    app = QApplication(sys.argv)
    form = Interface_Ombre()
    form.connect()
    form.show()
    app.exec_()

if __name__ == '__main__':
    main()

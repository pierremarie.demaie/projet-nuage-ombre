#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on %(date)s

@author: %(username)s
"""

import numpy as np

class Demicyl():
  
    def __init__(self,orig,rayon,direction): 
        self.origine = orig
        self.rayon = rayon
        self.direction = direction/np.linalg.norm(direction)
        
    def include(self,MP):
        """Fonction de test d'appartenance: le point doit être différent de l'origine,/
        il doit se trouver dans la bonne direction et être dans les limites du cylindre"""
        if (MP == [0,0,0]).all():
            return False
        MPu = np.dot(self.direction,MP)
        if MPu < 0:
            return False
        else:
            r = np.linalg.norm(np.cross(MP,self.direction))
            if r < self.rayon:
                return True
            else: return False

    
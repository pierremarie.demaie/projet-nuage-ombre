#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on %(date)s

@author: %(username)s
"""
import gpsdatetime as g
import demicyl
import numpy as np
if __name__=="__main__":
    #DEFINITION
    tic = g.gpsdatetime()
    p1=[1,1,1]
    p2=[-1,-1,-1]
    p3=[2,2,2]
    p4=[7,1,1]
    matpoints=np.vstack((p1,p2,p3,p4))
    print(matpoints)
    diametre,direction=3,[1,1,1]
    dc1=demicyl.Demicyl(p1,diametre,direction)
    vecteur=matpoints-matpoints[0]

    #TESTS
    print(dc1.rayon)#3
    print(np.linalg.norm(dc1.direction))#[1,1,1] normalisé
    for i in range(len(matpoints)):
        print(dc1.include(vecteur[i]))
        print(vecteur[i])
    #false
    #false
    #true
    #false

    
    
    
    toc =  g.gpsdatetime()
    dt = toc-tic
    print("Duration : %.3fs" % dt)

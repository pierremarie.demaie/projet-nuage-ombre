#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on %(date)s

@author: %(username)s
"""
import time as t
import ref
#import open3d as o3d #ne donne accès qu'aux points (pas de rgb/header)
from plyfile import PlyData, PlyElement

import numpy as np
if __name__=="__main__":
    
    tic = t.time()
    print("tic")
    #path="Ply/Chapelle-exterieur.ply"
    path='Chapelle-exterieur-sub.ply'   # a mettreheader en ascii
    date,heure='11-02-2000','00:00:00.123456789'
    rayon,alpha,uR,uL,origine=0,10,2,2,1
    t0,m0=0,2
    test=ref.Ref(date,heure,[0,0,0],path,rayon,alpha,uR,uL,t0,m0,origine)
    print(test.ouvrir_nuage())
    test.process_nuage()
    test.ombrage()
    test.creer_nuages()

    
    
    
    
    
    toc =  t.time()
    print('toc')
    dt = toc-tic
    print("Duration : %.3fs" % dt)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on %(date)s

@author: %(username)s
"""
import demicyl
import cone
import crayon
import numpy as np

class Nuage():
  
    def __init__(self,points,modele_ombre): 
        self.nbpoints=len(points)
        self.matpoints=points
        self.ombre=np.zeros((self.nbpoints,1))
        self.modele=modele_ombre
    
    def demicylindre(self,point,rayon,direction):
        """Crée le demicylindre associé à un point
        :param point: point d'origine du cylindre, déterminé par 3 coordonnées x,y et z
        :param rayon: le rayon du cylindre d'ombre en m
        :param direction: la direction du soleil, sous la forme d'un vecteur de 3 composantes x,y et z
        :type point: array [x,y,z] 
        :type rayon: float
        :type direction: array [x,y,z]
        :return: demicylindre d'ombre associé au point
        :rtype: demicyl
        """
        cyl=demicyl.Demicyl(point,rayon,direction)
        return cyl
    
    def cone_ombre(self,point,alpha,direction):
        """Crée le cone associé à un point
        :param point: point d'origine du cone, déterminé par 3 coordonnées x,y et z
        :param alpha: l'angle du cone d'ombre
        :param direction: la direction du soleil, sous la forme d'un vecteur de 3 composantes x,y et z
        :type point: array [x,y,z] 
        :type alpha: float
        :type direction: array [x,y,z]
        :return: cone d'ombre associé au point
        :rtype: cone
        """
        con=cone.Cone(point, alpha, direction)
        return con
    
    def crayon_ombre(self,point,alpha,rayon,direction):
        """Crée le crayon associé à un point
        :param point: point d'origine du cylindre, déterminé par 3 coordonnées x,y et z
        :param rayon: le rayon du demi-cylindre d'ombre en m
        :param alpha: l'angle du cone d'ombre
        :param direction: la direction du soleil, sous la forme d'un vecteur de 3 composantes x,y et z
        :type point: array [x,y,z] 
        :type rayon: float
        :type alpha: float
        :type direction: array [x,y,z]
        :return: crayon d'ombre associé au point
        :rtype: crayon
        """
        cray=crayon.Crayon(point, alpha, rayon, direction)
        return cray
                    
    def process(self,rayon,alpha,direction):
        """Crée et applique les modèles d'ombre des points du nuage aux autres points du nuages, pour calculer l'ombre
        :param rayon: le rayon du cylindre d'ombre en m
        :param direction: la direction du soleil, sous la forme d'un vecteur de 3 composantes x,y et z
        :param alpha: l'angle du cone d'ombre
        :type rayon: float
        :type alpha: float
        :type direction: array [x,y,z]
        """
        if self.modele == 0:
            for i in range(self.nbpoints):
                #création du demicylindre i
                demc_i =self.demicylindre(self.matpoints[i],rayon,direction)
                vecteur=self.vecteur(i)
                for j in range(self.nbpoints):
                    #application du demycilindre i au point j
                    if demc_i.include(vecteur[j]):
                        self.ombre[j]+=1
        elif self.modele == 1:  
            for i in range(self.nbpoints):
                #création du cone i
                cone_i =self.cone_ombre(self.matpoints[i],alpha,direction)
                vecteur=self.vecteur(i)
                for j in range(self.nbpoints):
                    #application du demycilindre i au point j
                    if cone_i.include(vecteur[j]):
                        self.ombre[j]+=1
        elif self.modele == 2:  
            for i in range(self.nbpoints):
                #création du crayon i
                cray_i =self.crayon_ombre(self.matpoints[i],rayon,alpha,direction)
                vecteur=self.vecteur(i)
                for j in range(self.nbpoints):
                    #application du demycilindre i au point j
                    if cray_i.include(vecteur[j]):
                        self.ombre[j]+=1
                        
    def vecteur(self,nb):
        """Calcule les vecteurs entre chaque point du nuage par rapport au point nb
        :param nb: numero du point 
        :type nb: int
        """
        return self.matpoints-self.matpoints[nb]

